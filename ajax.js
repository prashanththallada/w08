(function ($) {

    $('#btnLoadText').click(function () { $("#showResult").load("show.txt"); });
    $('#btnAjax').click(function () { callRestAPI() });
    $('.id1').css('background-color', 'rgba(29, 21, 21, 0.301)');
    // $('.id1').css('color', 'ghostwhite');
  
    // Perform an asynchronous HTTP (Ajax) API request.
    function callRestAPI() {
      var root = 'https://jsonplaceholder.typicode.com'
      $.ajax({
        url: root + '/posts/1',
        method: 'GET'
      }).then(function (response) {
        console.log(response);
        $('#showResult').html(response.body);
      });
    }
  })($);